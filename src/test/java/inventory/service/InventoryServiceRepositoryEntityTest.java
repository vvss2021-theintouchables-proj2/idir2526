package inventory.service;

import inventory.model.Inventory;
import inventory.model.OutsourcedPart;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryServiceRepositoryEntityTest {
    private OutsourcedPart outsourcedPart;
    private InventoryRepository inventoryRepository;
    private InventoryService inventoryService;

    @BeforeEach
    void setUp() {
        outsourcedPart = new OutsourcedPart(1, "part", 0.01, 2, 1, 4, "company.srl");
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
        inventoryRepository.addPart(outsourcedPart);
    }

    @Test
    void lookupPart() {
        assertEquals("part",inventoryService.lookupPart("part").getName());
    }

    @Test
    void deletePart() {
        int size = inventoryService.getAllParts().size();
        inventoryService.deletePart(outsourcedPart);
        assertEquals(size-1, inventoryService.getAllParts().size());
    }

}