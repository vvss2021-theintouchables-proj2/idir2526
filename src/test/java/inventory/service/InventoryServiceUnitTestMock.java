package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryServiceUnitTestMock {

    private OutsourcedPart outsourcedPart;

    private InventoryRepository inventoryRepository;

    private InventoryService inventoryService;

    @BeforeEach
    void setUp() {
        outsourcedPart = mock(OutsourcedPart.class);
        inventoryRepository = mock(InventoryRepository.class);
        inventoryService = new InventoryService(inventoryRepository);
        String name = "part";
        double price = 0.01;
        int inStock = 2;
        int min = 1;
        int  max = 4;
        String partDynamicValue = "company.srl";
        Mockito.when(outsourcedPart.getInStock()).thenReturn(inStock);
        Mockito.when(outsourcedPart.getName()).thenReturn(name);
        Mockito.when(outsourcedPart.getPrice()).thenReturn(price);
        Mockito.when(outsourcedPart.getMin()).thenReturn(min);
        Mockito.when(outsourcedPart.getMax()).thenReturn(max);
        Mockito.when(outsourcedPart.getCompanyName()).thenReturn(partDynamicValue);
    }

//    @Test
//    void addOutsourcePart() {
//        Mockito.doNothing().when(inventoryRepository).addPart(outsourcedPart);
//        inventoryService.addOutsourcePart(outsourcedPart.getName(), outsourcedPart.getPrice(), outsourcedPart.getInStock(), outsourcedPart.getMin(), outsourcedPart.getMax(), outsourcedPart.getCompanyName());
//        Mockito.verify(inventoryRepository, times(1)).addPart(outsourcedPart);
//    }

    @Test
    void lookupPart() {
        String name="part";
        Mockito.when(inventoryRepository.lookupPart(name)).thenReturn(outsourcedPart);
        inventoryService.lookupPart(name);
        Mockito.verify(inventoryRepository, times(1)).lookupPart(name);
    }

    @Test
    void deletePart() {
        Mockito.doNothing().when(inventoryRepository).deletePart(outsourcedPart);
        inventoryService.deletePart(outsourcedPart);
        Mockito.verify(inventoryRepository, times(1)).deletePart(outsourcedPart);
    }
}