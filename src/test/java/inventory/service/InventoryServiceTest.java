package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.validators.ValidationException;
import inventory.validators.ValidatorProduct;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@Tag("service")
@Tag("ECP-BVA")
class InventoryServiceTest {
    // 1 rulare + 1 bug fixed + 1 test de regresie + 1 rulare
    // 3 rulari + 2 bug fixed + 1 test regresie + 1 rulare
    private ValidatorProduct productValidator ;
    private InventoryRepository repo;
    private InventoryService service ;
    private ObservableList<Part> addParts ;
    private String filename = "data/items.txt";


    @BeforeEach
    void setUp() throws IOException {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        file.delete();
        file.createNewFile();
        productValidator = new ValidatorProduct();
        repo= new InventoryRepository();
        service = new InventoryService(repo);
        service.setValidatorProduct(productValidator);
        addParts = FXCollections.observableArrayList();
        addParts.add(new OutsourcedPart(1,"PlasticCup",2.45,3,2,10,"Phillips"));
        addParts.add(new OutsourcedPart(2,"PlasticCup v2.0",21.87,6,3,18,"Phillips"));
        }

    @AfterEach
    void tearDown() {
        // delete data from application
    }

//    @Timeout(value=1, unit= TimeUnit.SECONDS)
    @Tag("product")
    @Tag("add")
    @Tag("valid")
    @Test
    void addProduct_ECP_ValidPriceInStock_ProductAdded() {
        try {
            service.addProduct("NutriBullet",78.89,10,2,25, addParts);
            assertEquals(service.lookupProduct("NutriBullet").getName(),"NutriBullet");
            assert true;
        }catch (ValidationException e){
            assert false;
        }
    }


    @Tag("product")
    @Tag("add")
    @Tag("invalid")
    @DisplayName("InvalidPrice_ThrowsError \uD83D\uDEAB")
    @Test
    void addProduct_ECP_InvalidPrice_ExceptionThrown(){
        assertThrows(ValidationException.class, ()->{service.addProduct("NutriBullet",-7.56,10,2,25,addParts);});
    }

    @Tag("product")
    @Tag("add")
    @Tag("invalid")
    @DisplayName("InvalidInStock_ThrowsError \uD83D\uDEAB")
    @ParameterizedTest
    @ValueSource(ints = {-10,-31,-15})
    void addProduct_ECP_InvalidInStock_ExceptionThrown(int inStock){
        try {
            service.addProduct("NutriBullet",100.56,inStock,14,26,addParts);
            assert false;
        }catch (ValidationException e){
            assertEquals(e.getMessage(),"Inventory level must be greater than 0.\n");
            assert true;
        }
    }

    @Tag("product")
    @Tag("add")
    @Tag("invalid")
    @DisplayName("InvalidPrice_InvalidInStock_ThrowsError\uD83D\uDEAB")
    @RepeatedTest(3)
    void addProduct_ECP_InvalidPrice_InvalidInStock_ExceptionThrown(){
        try {
            service.addProduct("NutriBullet",-97.132,-56,6,19,addParts);
            assert false;
        }catch (ValidationException e){
            assertEquals(e.getMessage(),"The price must be greater than $0.\nInventory level must be greater than 0.\n");
            assert true;
        }
    }


    @Tag("product")
    @Tag("add")
    @Tag("valid")
    @DisplayName("ValidInStock_ProductAdded")
    @Test
    void addProduct_BVA_ValidInStock_ProductAdded(){
        try {
            service.addProduct("HairStraightener",50,2,1,12,addParts);
            assertEquals(2, service.lookupProduct("HairStraightener").getInStock());
            assert true;
        }catch (ValidationException e){
            assert false;
        }
    }

    @Tag("product")
    @Tag("add")
    @Tag("invalid")
    @DisplayName("InvalidPrice_ThrowsError \uD83D\uDEAB")
    @Test
    void addProduct_BVA_InvalidPrice_ExceptionThrown(){
        try {
            service.addProduct("CurlingIron",0.009,19,13,32,addParts);
            assert false;
        }catch (ValidationException e){
            assertEquals(e.getMessage(),"The price must be greater than $0.\n");
            assert true;
        }
    }

    @Timeout(value=1, unit= TimeUnit.SECONDS)
    @Tag("product")
    @Tag("add")
    @Tag("valid")
    @Test
    void addProduct_BVA_ValidPrice_ProductAdded() {
        try {
            service.addProduct("CurlingIron",0.01,4,3,11, addParts);
            assertEquals(0.01, service.lookupProduct("CurlingIron").getPrice());
            assert true;
        }catch (ValidationException e){
            assert false;
        }
    }

    @Tag("product")
    @Tag("add")
    @Tag("invalid")
    @DisplayName("InvalidPrice_ThrowsError \uD83D\uDEAB")
    @Test
    void addProduct_BVA_InvalidInStock_ExceptionThrown(){
        try {
            service.addProduct("CurlingIron",51,0,11,14, addParts);
            assert false;
        }catch (ValidationException e){
            assertEquals("Inventory level must be greater than 0.\n", e.getMessage());
            assert true;
        }
    }

}