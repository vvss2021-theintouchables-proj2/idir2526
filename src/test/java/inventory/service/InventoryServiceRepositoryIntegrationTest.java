package inventory.service;

import inventory.model.Inventory;
import inventory.model.OutsourcedPart;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryServiceRepositoryIntegrationTest {

    private OutsourcedPart outsourcedPart;
    private Inventory inventory;
    private InventoryRepository inventoryRepository;
    private InventoryService inventoryService;
    private String filename = "data/items.txt";

    @BeforeEach
    void setUp() throws IOException {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        file.delete();
        file.createNewFile();
        outsourcedPart = mock(OutsourcedPart.class);
        inventory = mock(Inventory.class);
        inventoryRepository = new InventoryRepository();
        inventoryRepository.setInventory(inventory);
        inventoryService = new InventoryService(inventoryRepository);
    }

    @Test
    void lookupPart() {
        String name="part";
        Mockito.when(inventory.lookupPart(name)).thenReturn(outsourcedPart);
        inventoryService.lookupPart(name);
        Mockito.verify(inventory, times(1)).lookupPart(name);
    }

    @Test
    void deletePart() {
        Mockito.doNothing().when(inventory).deletePart(outsourcedPart);
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableArrayList());
        inventoryService.deletePart(outsourcedPart);
        Mockito.verify(inventory, times(1)).deletePart(outsourcedPart);
    }
}