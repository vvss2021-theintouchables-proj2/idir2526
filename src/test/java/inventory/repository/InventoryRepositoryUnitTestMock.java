package inventory.repository;

import inventory.model.Inventory;
import inventory.model.OutsourcedPart;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryRepositoryUnitTestMock {

    private OutsourcedPart outsourcedPart;

    private Inventory inventory;

    private InventoryRepository inventoryRepository;

    @BeforeEach
    void setUp() {
        //MockitoAnnotations.initMocks(this);
        outsourcedPart = mock(OutsourcedPart.class);
        inventory = mock(Inventory.class);
        inventoryRepository = new InventoryRepository();
        inventoryRepository.setInventory(inventory);
        String name = "part";
        double price = 0.01;
        int inStock = 2;
        int min = 1;
        int  max = 4;
        String partDynamicValue = "company.srl";
        Mockito.when(outsourcedPart.getInStock()).thenReturn(inStock);
        Mockito.when(outsourcedPart.getName()).thenReturn(name);
        Mockito.when(outsourcedPart.getPrice()).thenReturn(price);
        Mockito.when(outsourcedPart.getMin()).thenReturn(min);
        Mockito.when(outsourcedPart.getMax()).thenReturn(max);
        Mockito.when(outsourcedPart.getCompanyName()).thenReturn(partDynamicValue);
    }

    @Test
    void lookupPart() {
        String name="part";
        Mockito.when(inventory.lookupPart(name)).thenReturn(outsourcedPart);
        inventoryRepository.lookupPart(name);
        Mockito.verify(inventory, times(1)).lookupPart(name);
    }

    @Test
    void deletePart() {
        Mockito.doNothing().when(inventory).deletePart(outsourcedPart);
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableArrayList());
        inventoryRepository.deletePart(outsourcedPart);
        Mockito.verify(inventory, times(1)).deletePart(outsourcedPart);
    }
}