package inventory.model;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
@Tag("model")
class InventoryTest {

    private Inventory inventory;
    @BeforeEach
    void setUp() {
        inventory=new Inventory();
    }

    @Tag("F02_TC01")
    @Test
    void lookUpPart_F02_TC01()
    {
        assertNull(inventory.lookupPart("!a"));
    }
    @Tag("F02_TC02")
    @Test
    void lookUpPart_F02_TC02()
    {
        assertNull(inventory.lookupPart("ab*cd"));
    }
    @Tag("F02_TC03")
    @Test
    void lookUpPart_F02_TC03()
    {
        assertNull(inventory.lookupPart("abcd!"));
    }

    @Test
    @Tag("F02_TC04")
    void lookupPart_FO2_TC04() {
        Part p1=new OutsourcedPart(1, "part", 12, 5, 1, 10, "sc.company");
        Part p2=new OutsourcedPart(2, "box", 12, 5, 1, 10, "sc.company");
        ObservableList<Part> parts= FXCollections.observableArrayList();
        parts.addAll(p1, p2);
        inventory.setAllParts(parts);
        Part p=inventory.lookupPart("pa");
        assertEquals("part", p.getName());
        assertEquals(1, p.getPartId());
    }

    @Test
    @Tag("F02_TC05")
    void lookupPart_FO2_TC05() {
        Part p1=new OutsourcedPart(1, "part", 12, 5, 1, 10, "sc.company");
        Part p2=new OutsourcedPart(2, "box", 12, 5, 1, 10, "sc.company");
        Part p3=new OutsourcedPart(3, "box2", 12, 5, 1, 10, "sc.company");
        Part p4=new OutsourcedPart(4, "box3", 12, 5, 1, 10, "sc.company");
        ObservableList<Part> parts= FXCollections.observableArrayList();
        parts.addAll(p1, p2, p3, p4);
        inventory.setAllParts(parts);
        Part p=inventory.lookupPart("2");
        assertEquals("box", p.getName());
        assertEquals(2, p.getPartId());
    }

    @Test
    @Tag("F02_TC06")
    void lookupPart_FO2_TC06() {
        Part p1=new OutsourcedPart(1, "part", 12, 5, 1, 10, "sc.company");
        Part p2=new OutsourcedPart(2, "box", 12, 5, 1, 10, "sc.company");
        Part p3=new OutsourcedPart(3, "box3", 12, 5, 1, 10, "sc.company");
        Part p4=new OutsourcedPart(4, "box1", 12, 5, 1, 10, "sc.company");
        ObservableList<Part> parts= FXCollections.observableArrayList();
        parts.addAll(p1, p2, p3, p4);
        inventory.setAllParts(parts);
        Part p=inventory.lookupPart("3");
        assertEquals("box3", p.getName());
        assertEquals(3, p.getPartId());
    }

    @Tag("F02_TC07")
    @Test
    void lookupPart_F02_TC07(){
        Part p1 = new OutsourcedPart(1,"box1",2.3,5,1,10,"Nintendo");
        Part p2 = new OutsourcedPart(2,"part",3.89,2,2,21,"PlayStation");
        Part p3 = new OutsourcedPart(3,"box3",9.87,3,1,32,"Xbox");
        Part p4 = new OutsourcedPart(3,"part2",3.21,3,1,32,"Xbox");
        ObservableList<Part> newParts = FXCollections.observableArrayList();
        newParts.addAll(p1,p2,p3,p4);
        inventory.setAllParts(newParts);
        assertNull(inventory.lookupPart("qwz"));
    }

    @Tag("F02_TC08")
    @Test
    void lookupPart_F02_TC08(){
        Part p1 = new OutsourcedPart(1,"part1",5.98,3,2,23,"Nintendo");
        Part p2 = new OutsourcedPart(2,"part",3.64,1,1,21,"PlayStation");
        Part p3 = new OutsourcedPart(3,"box3",9.43,5,3,14,"Xbox");
        Part p4 = new OutsourcedPart(3,"part2",1.98,4,2,76,"Xbox");
        Part p5 = new OutsourcedPart(3,"line",10.45,2,1,21,"Samsung");
        ObservableList<Part> newParts = FXCollections.observableArrayList();
        newParts.addAll(p1,p2,p3,p4,p5);
        inventory.setAllParts(newParts);
        assertEquals(p3,inventory.lookupPart("box"));
    }


    @Tag("F02_TC09")
    @Test
    void lookupPart_F02_TC09(){
        assertNull(inventory.lookupPart(""));
    }

}
