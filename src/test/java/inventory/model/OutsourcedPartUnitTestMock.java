package inventory.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OutsourcedPartUnitTestMock {

    private OutsourcedPart outsourcedPart;

    @BeforeEach
    void setUp() {
        outsourcedPart = new OutsourcedPart(1, "part", 0.01, 2, 1, 4, "company.srl");
    }

    @Test
    void getCompanyName() {
        assertEquals("company.srl", outsourcedPart.getCompanyName());
    }

    @Test
    void setCompanyName() {
        outsourcedPart.setCompanyName("companie2");
        assertEquals("companie2", outsourcedPart.getCompanyName());
    }
}