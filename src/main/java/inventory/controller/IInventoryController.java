package inventory.controller;

import inventory.service.InventoryService;
import inventory.validators.ValidatorPart;
import inventory.validators.ValidatorProduct;

public interface IInventoryController {
    void setServiceValidator(InventoryService service, ValidatorPart validatorPart, ValidatorProduct validatorProduct);
}
