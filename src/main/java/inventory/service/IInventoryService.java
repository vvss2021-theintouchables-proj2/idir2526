package inventory.service;

import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.ObservableList;

public interface IInventoryService {
    void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue);
    void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue);
    void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts);
    ObservableList<Part> getAllParts();
    ObservableList<Product> getAllProducts();
    Part lookupPart(String search);
    Product lookupProduct(String search);
    void updateInhousePart(int partIndex, int partId, String name, double price, int inStock, int min, int max, int partDynamicValue);
    void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue);
    void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts);
    void deletePart(Part part);
    void deleteProduct(Product product);
}
