package inventory.validators;



public class ValidatorPart{


    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     * @param name
     * @param price
     * @param inStock
     * @param min
     * @param max
     * @return
     */
    public void isValidPart(String name, double price, int inStock, int min, int max) throws ValidationException{
        String errorMessage="";
        if(name.equals("")) {
            errorMessage += "A name has not been entered.\n";
        }
        if(price < 0.01) {
            errorMessage += "The price must be greater than 0.\n";
        }
        if(inStock < 1) {
            errorMessage += "Inventory level must be greater than 0.\n";
        }
        if(min > max) {
            errorMessage += "The Min value must be less than the Max value.\n";
        }
        if(inStock < min && inStock >= 1) {
            errorMessage += "Inventory level is lower than minimum value.\n";
        }
        if(inStock > max && inStock >= 1) {
            errorMessage += "Inventory level is higher than the maximum value.\n";
        }
        if (errorMessage.length()!=0)
            throw new ValidationException(errorMessage);
    }
}
