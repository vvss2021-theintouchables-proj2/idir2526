package inventory.validators;

import inventory.model.Part;
import javafx.collections.ObservableList;

public class ValidatorProduct{

    /**
     * Generate an error message for invalid values in a product
     * and evaluate whether the sum of the price of associated parts
     * is less than the price of the resulting product.
     * A valid product will return an empty error message string.
     * @param name
     * @param min
     * @param max
     * @param inStock
     * @param price
     * @param parts
     * @return
     */
    public void isValidProduct(String name, double price, int inStock, int min, int max, ObservableList<Part> parts) throws ValidationException {
        String errorMessage = "";
        double sumOfParts = 0.00;
        for (int i = 0; i < parts.size(); i++) {
            sumOfParts += parts.get(i).getPrice();
        }
        if (name.equals("")) {
            errorMessage += "A name has not been entered.\n";
        }
        if (min < 0) {
            errorMessage += "The inventory level must be greater than 0.\n";
        }
        if (price < 0.01) {
            errorMessage += "The price must be greater than $0.\n";
        }
        if(inStock < 1) {
            errorMessage += "Inventory level must be greater than 0.\n";
        }
        if (min > max) {
            errorMessage += "The Min value must be less than the Max value.\n";
        }
        if(inStock < min && inStock >= 1) {
            errorMessage += "Inventory level is lower than minimum value.\n";
        }
        if(inStock > max && inStock >= 1) {
            errorMessage += "Inventory level is higher than the maximum value.\n";
        }
        if (parts.isEmpty()) {
            errorMessage += "Product must contain at least 1 part.\n";
        }
        if (sumOfParts > price && price > 0.01) {
            errorMessage += "Product price must be greater than cost of parts.\n";
        }
        if (errorMessage.length()!=0)
            throw new ValidationException(errorMessage);
    }

}
